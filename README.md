# Raspberry-birdsnest
Instructions, patches, links and documentation for setting up zoneminder using  a PAL-camera, v4l2, an easycap USB converter, gstreamer, and a raspberry-pi.

This project is not intended to produce a lot of code. It is rather an attempt to store information that may come in handy. It's like a blog-post to share stuff, but with the added benefit that others can contribute to keep things up to date.
